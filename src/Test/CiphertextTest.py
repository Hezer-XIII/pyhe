from crypto.Ciphertext import Ciphertext
import unittest


class CiphertextTest(unittest.TestCase):

    def test_apply(self):
        c = Ciphertext(3, 11, [1,2,3])
        self.assertEqual(c.apply(lambda x : sum(x.M)), 6)

    def test_refresh(self):
        pass
