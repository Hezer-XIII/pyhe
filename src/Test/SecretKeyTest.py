from crypto.SecretKey import ESecretKey, FHESecretKey
from crypto.PublicKey import EPublicKey, FHEPublicKey
from crypto.Ciphertext import Ciphertext
import unittest
import random
import math


class SecretKeyTest(unittest.TestCase):

    def test_ESK_decrypt(self):
        sk = ESecretKey(2, 11)
        pk = EPublicKey(4, 2, 11, sk)
        c  = pk.encrypt(1)
        self.assertEqual(sk.decrypt(c), 1)

    def test_FHESK_decrpyt(self):
        L = 1
        n1 = random.randint(1,3)
        n2 = random.randint(1,3)
        q1 = 11
        q2 = 11
        N1 = math.ceil((2*n1+1)*math.log(q1,2))
        N2 = math.ceil((2*n2+1)*math.log(q2,2))
        sk = FHESecretKey([n1,n2], [q1,q2], L)
        pk = FHEPublicKey([N1,N1], [n1,n2], [q1,q2], sk, L)
        c = pk.encrypt(1)
        self.assertEqual(sk.decrypt(c), 1)
