from utils.aVector import aVector
import numpy as np
import unittest



class aVectorTest(unittest.TestCase):

    def test_tensorProduct(self):
        V1 = aVector(3,[1,2,3])
        V2 = aVector(2,[1,2])
        V = V1.tensorProduct(V2)
        self.assertEqual(V1, aVector(3,[1,2,3]))
        self.assertEqual(V2, aVector(2,[1,2]))
        self.assertEqual(V , aVector(6,[1,2,2,4,3,6]))
        self.assertEqual(V.m, V1.m*V2.m)
        self.assertEqual(V.n, 1)

    def test_bitDecomp(self):
        V1 = aVector(2,[1,2])
        V2 = V1.bitDecomp(q=5)
        self.assertEqual(V1, aVector(2, [1,2]))
        self.assertEqual(V2, aVector(6, [1,0,0,1,0,0]))

    def test_powerof2(self):
        V1 = aVector(2,[1,2])
        V1 = V1.powerof2(q=5)
        self.assertEqual(V1, aVector(6, [1,2,2,4,4,3]))

    def test_equiv(self):
        q=5
        V1 = aVector(3,[1,2,2])
        V2 = aVector(3,[1,3,4])
        V1_ = V1.bitDecomp(q=q)
        V2_ = V2.powerof2(q=q)
        self.assertEqual(V1.transpose()*V2%q, V1_.transpose()*V2_%q)
