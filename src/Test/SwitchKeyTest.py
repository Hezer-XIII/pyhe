from crypto.SecretKey import ESecretKey, FHESecretKey
from crypto.PublicKey import EPublicKey, FHEPublicKey
from crypto.SwitchKey import ESwitchKey, FHESwitchKey
from crypto.Ciphertext import Ciphertext
import unittest
import random
import math


class SwitchKeyTest(unittest.TestCase):

    def test_init(self):
        sk_1 = ESecretKey(3, 16)
        sk_2 = ESecretKey(2, 8)
        swk = ESwitchKey(sk_1, sk_2)
        self.assertEqual(swk.m, sk_1.m*math.ceil(math.log(sk_1.q,2)))
        self.assertEqual(swk.n, sk_2.m)

    def test_SwitchKey(self):
        sk_1 = ESecretKey(3, 16)
        sk_2 = ESecretKey(2, 8)
        pk_1 = EPublicKey(8, 3, 16, sk_1)
        pk_2 = EPublicKey(6, 2, 8 , sk_2)
        swk = ESwitchKey(sk_1, sk_2)
        c = pk_1.encrypt(1)
        c = swk.SwitchKey(c, 8)
        self.assertEqual(sk_2.decrypt(c),1)
