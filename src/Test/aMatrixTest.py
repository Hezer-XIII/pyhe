from utils.aMatrix import aMatrix
import numpy as np
import unittest


class aMatrixTest(unittest.TestCase):
    """docstring for aMatrixTest."""

    def test_transpose(self):
        A = aMatrix(2,3,[1,2,3,4,5,6])
        B = A.transpose()
        self.assertEqual(A.m,2)
        self.assertEqual(A.n,3)
        self.assertEqual(A.M[0][1], 2)
        self.assertEqual(B.m,3)
        self.assertEqual(B.n,2)
        self.assertEqual(B.M[0][1], 4)

    def test_eq(self):
        A = aMatrix(2,2,[2,2,2,2])
        B = aMatrix(2,2,[2,2,2,2])
        C = aMatrix(2,2,[2,2,2,1])
        self.assertEqual(A,B)
        self.assertNotEqual(A,C)

    def test_add(self):
        A = aMatrix(2,2,[2,2,2,2])
        B = aMatrix(2,2,[1,2,1,2])
        C = A+B
        self.assertEqual(A, aMatrix(2,2,[2,2,2,2])) # Check if A is modify
        self.assertEqual(B, aMatrix(2,2,[1,2,1,2])) # Check if B is modify
        self.assertEqual(C, aMatrix(2,2,[3,4,3,4])) # Check if A+B is correct


    def test_mul_mat(self):
        A = aMatrix(2,2,[2,2,2,2])
        B = aMatrix(2,2,[1,2,1,2])
        C = A*B
        self.assertEqual(A, aMatrix(2,2,[2,2,2,2])) # Check if A is modify
        self.assertEqual(B, aMatrix(2,2,[1,2,1,2])) # Check if B is modify
        self.assertEqual(C, aMatrix(2,2,[4,8,4,8])) # Check if A*B is correct

    def test_mul_int(self):
        A = aMatrix(2,2,[2,2,2,2])
        B = A*2
        self.assertEqual(A, aMatrix(2,2,[2,2,2,2])) # Check if A is modify
        self.assertEqual(B, aMatrix(2,2,[4,4,4,4])) # Check if A*2 is correct

    def test_mul_np(self):
        A = aMatrix(2,2,[2,2,2,2])
        B = np.array([[1,2],[1,2]])
        C = A*B
        self.assertEqual(A, aMatrix(2,2,[2,2,2,2])) # Check if A is modify
        self.assertEqual(C, aMatrix(2,2,[4,8,4,8])) # Check if A*B is correct
