from utils.aVector import aVector
import math


class Ciphertext(aVector):
    """docstring for Ciphertext."""
    def __init__(self, n, q, M, L=0):
        super(Ciphertext, self).__init__(n,M)
        #self.M = M % q
        self.q = q
        self.L = L

    def apply(self, func):
        return func(self)

    def modify(self, func):
        #test = func(self)
        #print(test)
        self = func(self)#.transpose()

    def refresh(self, T):
        if self.L < 0:
            print("Impossible to refresh more!")
            return None
        c = self.powerof2()
        c = c.scale(c.q, T.next_q(c.L), 2)
        c.L -= 1
        return T.SwitchKey(c)

    def tensorProduct(self,other):
        return Ciphertext(self.m*other.m, self.q, super().tensorProduct(other), self.L)

    def powerof2(self):
        return Ciphertext(self.m*math.ceil(math.log(self.q,2)), self.q, super().powerof2(), self.L)

    def bitDecomp(self, q=None):
        if not q:
            q = self.q
        return Ciphertext(self.m*math.ceil(math.log(q,2)), self.q, super().bitDecomp(q=q), self.L)

    def __mul__(self,other):
        return self.tensorProduct(other)

    def mul2(self,other):
        new_M = [ (a*b)%self.q for a,b in zip(self.M,other.M) ]
        return Ciphertext(self.m, self.q, new_M, self.L)

    def scale(self, q1, q2, r):
        new_c = super().scale(q1, q2, r)
        return Ciphertext(new_c.m, q2, new_c, self.L) % q2

    def __mod__(self, other):
        new_c = super().__mod__(other)
        return Ciphertext(self.m, self.q, new_c, self.L)
