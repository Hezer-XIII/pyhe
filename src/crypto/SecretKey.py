from utils.utils import bitDecompInt
from utils.aVector import aVector
import numpy as np
import math
import random
import copy



class ESecretKey(aVector):
    """BGV Secret Key.

    Parameters
    ----------
    n : int
        n+1 is the size of the Private Key.
        See BGV documentation.

    q : int
        The modulus.
        All element in the key are in R_q.

    p : int
        The secret message are in R_p.
    """

    def __init__(self, n, q, p=2, M=None):
        """
        @param  n   size of key vector = n+1
        @param  q   modulus (element of the key are inside R_q)
        """
        self.q = q
        self.p = p
        if not M:
            #_S = [[ random.randint(0, int(q/4)) for _ in range(1)  ]
            #                                    for _ in range(n) ]  #TODO change random generator
            _S = [[ abs(int(np.random.normal(scale=3))) for _ in range(1)  ]
                                                   for _ in range(n) ]  #TODO change random generator
            M = np.array([[1]] + _S)
        super(ESecretKey, self).__init__(n+1, M)

    def decrypt(self, c):
        """Decrypt the Ciphertext c into µ in R_p.

        Parameters
        ----------
        c : crypto.Ciphertext
            The Ciphertext that encrypt µ.
            The used Public Key for c must be generated with current Secret Key.

        Returns
        -------
        µ : int
            The encrypted message in R_p.
        """
        return c.apply( lambda x : (x.transpose() * self % self.q) % self.p)[0][0]
        #return c.apply( lambda x : (x.transpose() * self ) )[0][0]

    def tensorProduct(self, other):
        """Apply the tensor product of two Secret Key a return a new one.

        Parameters
        ----------
        other : crypto.ESecretKey
            Another Secret Key with same size (n+1).

        Returns
        -------
        s' : crypto.ESecretKey
            A new Secret Key of size (n+1)^2.
        """
        return ESecretKey(self.m*other.m-1, self.q, self.p, M=super().tensorProduct(other))

    def bitDecomp(self):
        """Apply the BitDecomp algorithm on the Secret Key a return a new one.

        Returns
        -------
        u : crypto.ESecretKey
            A new Secret Key in R_2^{(n+1).\lceil \log_2 q \rceil}.
        """
        return ESecretKey(self.m*math.ceil(math.log(self.q,2))-1, self.q, self.p, M=super().bitDecomp())

    def __mul__(self, other):
        return self.tensorProduct(other)

    def __str__(self):
        return str(self.M)+" ("+str(self.m)+"x"+str(self.n)+") (q="+str(self.q)+")"




class FHESecretKey(object):
    """BGV Levelled Secret Key.

    Parameters
    ----------
    n_tab : array of int of size L+1
        The size of each ESecretKey inside the FHESecretKey.
        See BGV documentation.

    q_tab : array of int of size L+1
        The modulus of each ESecretKey inside the FHESecretKey.

    L : int
        The Level of the Secret Key.
        A L-level FHE.Key have L+1 private/public key.
        A L-level FHE.Key can refresh a Ciphertext L times
            to made mult and add without Bootstrapping.
        See BGV documentation.

    p : int
        The secret message are in R_p.
    """

    def __init__(self, n_tab, q_tab, L, p=2):
        super(FHESecretKey, self).__init__()
        self.L = L
        self.q_tab = q_tab
        self.n_tab = n_tab
        self.p = p
        self.S_j = [None]*(L+1)
        for j in range(self.L,-1,-1):
            self.S_j[j] = ESecretKey(n_tab[j], q_tab[j], p)

    def __str__(self):
        res = ""
        for j in range(self.L,-1,-1):
            res += "S["+str(j)+"]= "+str(self.S_j[j])+"\n"
        return res

    def decrypt(self, c):
        """Decrypt the Ciphertext c into µ in R_p.
        Use the ESecretKey of at the level L (from Ciphertext).

        Parameters
        ----------
        c : crypto.Ciphertext
            The Ciphertext that encrypt µ.
            c must be encrypted with corresponding FHEPublicKey.

        Returns
        -------
        µ : int
            The encrypted message in R_p.
        """
        return self.S_j[c.L].decrypt(c)

    def __mul__(self,other):
        return self.S_j[self.L].tensorProduct(other.S_j[other.L])
