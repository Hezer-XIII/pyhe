from crypto.PublicKey import EPublicKey
from crypto.Ciphertext import Ciphertext
from utils.aMatrix import aMatrix
import math
import copy


class ESwitchKey(aMatrix):
    """docstring for SwitchKey."""
    def __init__(self, sk_1, sk_2, p=2):
        sk_1 = copy.deepcopy(sk_1)
        sk_2 = copy.deepcopy(sk_2)
        N = sk_1.m*math.ceil(math.log(sk_1.q,2))
        M = EPublicKey(N, sk_2.m-1, sk_2.q, sk_2, p)
        sk_1 = sk_1.powerof2()
        M = M.addVector(sk_1) % sk_2.q
        super(ESwitchKey, self).__init__(N, sk_2.m, M)

    def SwitchKey(self, c, q):
        #c.bitDecomp()
        #print(c)
        return Ciphertext(self.n, q, c.apply(lambda x : x.bitDecomp(q*2).transpose() * self), c.L)

    def __str__(self):
        return "\n"+str(self.M)+"("+str(self.m)+"x"+str(self.n)+")"


class FHESwitchKey(object):
    """docstring for FHESwitchKey."""
    def __init__(self, n_tab, q_tab, sk, L, p=2):
        super(FHESwitchKey, self).__init__()
        self.L = L
        self.q_tab = q_tab
        self.n_tab = n_tab
        self.p = p
        self.T_j = [0]*L      #T[j] = cipher over sk[j+1] ==> cipher over sk[j]
        for j in range(L-1,-1,-1):
            self.T_j[j] = ESwitchKey(sk.S_j[j+1].tensorProduct(sk.S_j[j+1]).bitDecomp(), sk.S_j[j], p)
        #self.L -= 1

    def __str__(self):
        res = ""
        for j in range(self.L-1,-1,-1):
            res += "T[sk["+str(j+1)+"]->sk["+str(j)+"]]= "+str(self.T_j[j])+"\n"
        return res

    def SwitchKey(self, c):
        if c.L < 0:
            print("Impossible to refresh more!")
            return None
        #self.L = c.L
        #print("new q=", self.q_tab[self.L])
        new_c = self.T_j[c.L].SwitchKey(c, self.q_tab[c.L])
        #self.L -= 1
        return new_c

    def next_q(self, l):
        return self.q_tab[l-1]
