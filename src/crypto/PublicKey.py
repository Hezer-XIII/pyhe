from crypto.Ciphertext import Ciphertext
from utils.aMatrix import aMatrix
from utils.aVector import aVector
import numpy as np
import random
import math


class EPublicKey(aMatrix):
    """BGV Public Key.

    Parameters
    ----------
    N : int
        The number of line for the Matrix.
        See BGV documentation.

    n : int
        The number of column for the Matrix.
        n+1 is also the size of the Private Key.
        See BGV documentation.

    q : int
        The modulus.
        All element in the key are in R_q.

    sk : ESecretKey
        The Secret Key used for decryption.
        The Secret Key sk is only able to decrypt a ciphertext generated with
        this Public Key.

    p : int
        The secret message are in R_p.
    """

    def __init__(self, N, n, q, sk, p=2):
        self.q = q
        self.p = p
        #self.n = n+1
        #self.N = N
        M = np.array([[ random.randint(0, q-1) for _ in range(n) ]
                                                for _ in range(N) ])
        e  = np.array([ [ random.randint(0, int((math.ceil((q-1)/p)-1)/N))
                                                        for _ in range(1) ]
                                                        for _ in range(N) ])
        #e  = np.array([[ abs(int(np.random.normal(scale=0))) for _ in range(1) ]
        #                                                for _ in range(N) ])
        #print(e)
        b = (M.dot(sk.M[1:]) + self.p*e) % self.q
        M = (np.array([ np.concatenate((b[i],-M[i])) for i in range(N) ])) % self.q
        super(EPublicKey, self).__init__(N, n+1, M)

    def __str__(self):
        return "\n"+str(self.M)+"("+str(self.m)+"x"+str(self.n)+") (q="+str(self.q)+")"

    def encrypt(self, µ, L=0):
        """Encrypt the message µ in R_p.

        Parameters
        ----------
        µ : int
            The message in R_p.

        L : int
            The current Level of the returned Ciphertext.
            See BGV documentation.

        Returns
        -------
        c : crypto.Ciphertext
            The Ciphertext that encrypt the message µ under the key sk,pk.
        """
        m = aVector(self.n, [µ]+[0]*(self.n-1))
        r = aVector(self.m, [ random.randint(0,self.p-1) for _ in range(self.m) ])
        A = aMatrix(self.m, self.n, self.M)
        A = A.transpose()
        A *= r
        A += m
        A %= self.q
        return Ciphertext(self.n, self.q, A, L)


class FHEPublicKey(object):
    """BGV Levelled Public Key.

    Parameters
    ----------
    N_tab : array of int of size L+1
        The value of N for each EPublicKey inside the FHEPublicKey.
        See BGV documentation.

    n_tab : array of int of size L+1
        The value of n for each EPublicKey inside the FHEPublicKey.
        See BGV documentation.

    q_tab : array of int of size L+1
        The modulus for each EPublicKey inside the FHEPublicKey.

    sk : FHESecretKey
        The Secret Key used for decryption.
        Each ESecretKey inside this FHESecretKey will be used to generated
            each EPublicKey inside this FHEPublicKey.

    L : int
        The Level of the Public Key.
        A L-level FHE.Key have L+1 private/public key.
        A L-level FHE.Key can refresh a Ciphertext L times
            to made mult and add without Bootstrapping.
        See BGV documentation.

    p : int
        The secret message are in R_p.
    """
    def __init__(self, N_tab, n_tab, q_tab, sk, L, p=2):
        super(FHEPublicKey, self).__init__()
        self.L = L
        self.q_tab = q_tab
        self.n_tab = n_tab
        self.N_tab = N_tab
        self.p = p
        self.A_j = [0]*(L+1)
        for j in range(L,-1,-1):
            self.A_j[j] =  EPublicKey(N_tab[j],n_tab[j],q_tab[j],sk.S_j[j],p)

    def __str__(self):
        res = ""
        for j in range(self.L,-1,-1):
            res += "A["+str(j)+"]= "+str(self.A_j[j])+"\n"
        return res

    def encrypt(self, µ):
        """Encrypt the message µ in R_p.

        Parameters
        ----------
        µ : int
            The message in R_p.

        Returns
        -------
        c : crypto.Ciphertext
            The Ciphertext that encrypt the message µ under the key sk,pk
            with a level L.
        """
        return self.A_j[self.L].encrypt(µ, self.L)
