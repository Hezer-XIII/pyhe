

def bitDecompInt(i, res=[]):
    """Return a tabular with binary of i
    The element at index j is the j^th bit
    (0^th is at the left)
    """
    if i == 1:
        return res+[1]
    elif i == 0:
        return res+[0]
    elif i%2 == 1:
        return bitDecompInt(i//2, res+[1])
    elif i%2 == 0:
        return bitDecompInt(i//2, res+[0])
