from abc import ABC, abstractmethod
import numpy as np
import copy


class aMatrix(ABC):
    """docstring for aMatrix."""
    def __init__(self, m, n, M=None):
        super(aMatrix, self).__init__()
        # n X m
        self.m = m
        self.n = n
        if isinstance(M,(np.ndarray,list)):
            self.M = np.array(M).reshape((m,n))
        elif isinstance(M, aMatrix):
            self.M = M.M.reshape((m,n))

    def __str__(self):
        return str(self.M)+" ("+str(self.m)+"x"+str(self.n)+")"

    def __getitem__(self,key):
        return self.M[key]

    def __setitem__(self,key,value):
        self.M[key] = value

    def __mod__(self, other):
        return aMatrix(self.m,self.n,self.M%other)

    def __add__(self, other):
        new = copy.deepcopy(self)
        if isinstance(other, (int,np.ndarray)):
            new.M = self.M + other
        elif isinstance(other, aMatrix):
            new.M = self.M + other.M
        return new

    def __mul_mat(self, other):
        new = copy.deepcopy(self)
        new.M = self.M.dot(other.M)
        new.n = other.n
        return new

    def __mul_np(self, other):
        new = copy.deepcopy(self)
        new.M = self.M.dot(other)
        new.n = len(other[0])
        return new

    def __mul_int(self,other):
        # /!\ oly work with aMatrix * int, not the other side
        new = copy.deepcopy(self)
        new.M = new.M*other
        return new

    def __mul__(self,other):
        if isinstance(other, int):
            return self.__mul_int(other)
        elif isinstance(other, aMatrix):
            return self.__mul_mat(other)
        elif isinstance(other, np.ndarray):
            return self.__mul_np(other)

    """
    def __eq__(self,other):
        if self.m != other.m or self.n != other.n:
            return False
        for i in range(self.m):
            for j in range(self.n):
                if self.M[i][j] != other.M[i][j]:
                    return False
        return True
    """
    def __eq__(self,other):
        if self.m != other.m or self.n != other.n:
            return False
        return not False in (self.M == other.M)

    def transpose(self):
        return aMatrix(self.n, self.m, self.M.transpose())
        #self.M = self.M.transpose()
        #self.m,self.n = self.n,self.m

    def addVector(self, v, j=0):
        """
        Add a aVector of size N to the j^th columns of the current aMatrix
        """
        new_M = copy.deepcopy(self.M)
        for i in range(self.m):
            new_M[i][j] += v.M[i][0]
        return aMatrix(self.m, self.n, new_M)
