from abc import ABC, abstractmethod
from utils.utils import bitDecompInt
from utils.aMatrix import aMatrix
import numpy as np
import copy
import math


class aVector(aMatrix):
    """docstring for aVector."""
    def __init__(self, m, M=[]):
        super(aVector, self).__init__(m, 1, M)
        #self.q = 11 #TODO remove

    def __str__(self):
        return "\n"+str(self.M)+" ("+str(self.m)+"x"+str(1)+")"

    def tensorProduct(self, other):
        new_V = np.tensordot(self.M,other.M, 0).reshape(len(self.M)*len(other.M),1) #% self.q
        return aVector(self.m*other.m, M=new_V)

    def bitDecomp(self, q=None):
        if q==None:
            q=self.q
        new_V = []
        tmp = []
        decomp = []
        bit = math.ceil(math.log(q,2))
        for i in self.M:
            decomp = bitDecompInt(i)
            tmp += [decomp + [0 for _ in range(bit-len(decomp))]]
        for j in range(bit):
            for i in range(len(tmp)):
                new_V += [tmp[i][j]]
        #new_M = np.array([new_V]).reshape((len(new_V),1)) % q
        #self.m = self.m * math.ceil(math.log(q,2))
        return aVector(self.m*math.ceil(math.log(q,2)), new_V)

    def powerof2(self, q=None):
        if q==None:
            q=self.q
        new_M = copy.deepcopy(self.M)
        for exp in range(1,math.ceil(math.log(q,2))):
            new_M = np.concatenate((new_M, (2**exp)*self.M))
        #self.M = new_M % q
        #self.m = self.m * math.ceil(math.log(q,2))
        return aVector(self.m*math.ceil(math.log(q,2)), new_M) % q

    def scale(self, q1, q2, r):
        new_V = self.M * q2 // q1
        for j in range(len(new_V)):
            modulo = new_V[j][0] % r
            if modulo != self.M[j][0] % r:
                new_V[j][0] -= modulo + (self.M[j][0]%r)
        return aVector(self.m, new_V)
