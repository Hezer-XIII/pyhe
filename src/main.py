from crypto.PublicKey import EPublicKey, FHEPublicKey
from crypto.SecretKey import ESecretKey, FHESecretKey
from crypto.SwitchKey import ESwitchKey, FHESwitchKey
from utils.utils import bitDecompInt
from utils.aMatrix import aMatrix
from utils.aVector import aVector
import random
import math


def bit_to_num(bit_tab, p=2):
    res = 0
    exp = 0
    for bit in bit_tab:
        if bit != 0:
            res += bit*(p**exp)
        exp += 1
    return res

def bit_bit_add(c1, c2, const_1, const_0, swk, p=2):
    c = []
    rep = const_0
    if len(c2) > len(c1):
        c1,c2 = c2,c1   # c1 always longer than c2
    for i in range(len(c1)+1):
        if i == len(c1): #last round
            add = rep
        else:
            a = c1[i]
            b = (c2[i] if i<len(c2) else const_0)
            add = (a + b + rep) * const_1
            rep = ((a+rep) * (b+rep)) + (rep*const_1)
        c.append(add)
    return c


def bit_bit_add2(c1, c2, const_1, const_0, swk, sk):
    c = []
    cr = []
    rep = const_0
    cr.append(rep)
    if len(c2) > len(c1):
        c1,c2 = c2,c1
    for i in range(len(c1)+1):
        if i == len(c1):
            add = rep
        else:
            a = c1[i]
            b = (c2[i] if i<len(c2) else const_0)
            add = (a + b + rep) * const_1
            rep = ((a+rep) * (b+rep)) + (rep*const_1)
            add = add.refresh(swk)
            rep = rep.refresh(swk)
            for j in range(i+1, len(c1)):
                c1[j] = (c1[j]*const_1).refresh(swk)
                c2[j] = (c2[j]*const_1).refresh(swk)
            #for j in range(len(c)):
            #    c[j] = (c[j]*const_1).refresh(swk)
            #for j in range(len(cr)):
            #    cr[j] = (cr[j]*const_1).refresh(swk)
            const_1 = (const_1*const_1).refresh(swk)
            cr.append(rep)
        c.append(add)
        """
        print([ sk.decrypt(c_) for c_ in c1 ])
        print([ sk.decrypt(c_) for c_ in c2 ])
        print([ sk.decrypt(c_) for c_ in c ])
        print('-----')
        """
    return c,cr

if __name__ == '__main__':

    i = 0
    for _ in range(100):
        p = 2
        bit = 5
        µ1 = [ random.randint(0,p-1) for _ in range(bit) ]
        µ2 = [ random.randint(0,p-1) for _ in range(bit) ]
        L = bit
        n = [ 20 for _ in range(L+1)]
        q = [ 2**(3+i) for i in range(L+1)]
        N = [ math.ceil((2*n[i]+1)*math.log(q[i],2)) for i in range(L+1)]
        sk = FHESecretKey(n, q, L, p=p)
        pk = FHEPublicKey(N, n, q, sk, L, p=p)
        swk = FHESwitchKey(n, q, sk, L, p=p)


        #"""
        const_0 = pk.encrypt(0)
        const_1 = pk.encrypt(1)
        c1 = [ pk.encrypt(bit) for bit in µ1 ]
        c2 = [ pk.encrypt(bit) for bit in µ2 ]
        c,cr  = bit_bit_add2(c1,c2, const_1, const_0,swk, sk)

        m = [ sk.decrypt(c[i]) for i in range(len(c)) ]
        mr = [ sk.decrypt(cr[i]) for i in range(len(cr)) ]
        if bit_to_num(m) != bit_to_num(µ1)+bit_to_num(µ2) :
            i += 1
            print("error ******************")

        print("+",mr, " report")
        print(" ",µ1," (",bit_to_num(µ1,p),")")
        print("+",µ2," (",bit_to_num(µ2,p),")")
        print("---------------------")
        print("=",m," (",bit_to_num(m,p),")")
        print("=====================")
        #break
        """

        c1 = pk.encrypt(µ1[0])
        c2 = pk.encrypt(µ2[0])
        c  = c1*c2
        c  = c.refresh(swk)
        for _ in range(L-1):
            c *= c
            c  = c.refresh(swk)
        m  = sk.decrypt(c)
        if ((µ1[0]*µ2[0]))%p != m:
            i += 1
            print("ERROR", (µ1[0]*µ2[0])%p, "(",m,")"," ****")
            break
        else:
            print("GOOD ", (µ1[0]*µ2[0])%p, "(",m,")")
        #"""

    print(i," ERRORS")


# TODO
# - Réussir à faire un SwitchKey
